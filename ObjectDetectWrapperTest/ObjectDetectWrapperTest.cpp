#include <string>
#include <iostream>
#include <fstream>
#include <direct.h>
#include "windows.h"
#include "ObjectDetect.h"
#include "UtilFile.h"
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include "glog/logging.h"   // glog ͷ�ļ�

using namespace std;

bool DoesFileExist(string strFileName) {
	if (-1 != GetFileAttributesA(strFileName.c_str()))
		return true;
	else
		return false;
}

std::string GetFileNameByFilePathName(const std::string filePathName) {
	const int BUFSIZE = 4096;
	char buffer[BUFSIZE];
	char * lppPart = { NULL };
	DWORD retval = GetFullPathNameA(filePathName.c_str(), BUFSIZE, buffer, &lppPart);
	if (retval == 0) {
		// Handle an error condition.
		printf("GetFullPathName failed (%d)\n", GetLastError());
		return "";
	}

	std::string filename = buffer;
	if (filePathName.size() < 0)
		return "";
	string::size_type ix = filename.find_last_of('\\');

	if (ix != string::npos)
		filename = filename.substr(ix + 1, filename.size() - ix);

	return filename;
}

void BatchProcess(const char * pszImageDir) {
	vector<string> fileList;
	SearchImageFiles(fileList, pszImageDir, true);
	for (vector<string>::iterator it = fileList.begin(); it != fileList.end(); it++) {
		printf(">>> process: %s\n", it->c_str());
		char* pszRes = Detection(it->c_str(), 0);
		printf("%s\n<---\n", pszRes);
	}
}

void SaveImage(string strJSON, string strDstFile) {

}

void BatchProcess(string strImageDir, int missErrorRatio) {
	if (!DoesFileExist(strImageDir)) {
		std::cout << "src file directory doesn't exist: " << strImageDir << endl;
		return;
	}

	vector<string> fileList;
	SearchImageFiles(fileList, strImageDir.c_str(), true);
	if (0 == fileList.size()) {
		std::cout << "image file doesn't exist" << endl;
		return;
	}
	std::cout << "image file count: " << fileList.size() << endl;

	for (vector<string>::iterator it = fileList.begin(); it != fileList.end(); it++) {
		printf(">>> process: %s\n", it->c_str());
		char* pszRes = Detection(it->c_str(), missErrorRatio);
		printf("%s\n<---\n", pszRes);
	}
}

int main(int argc, char * argv[]) {
	::google::InitGoogleLogging(argv[0]);
	if (4 > argc) {
		printf_s("format: objectTypes\t configFile\t imgDir [imgDir...]\n");
		std::cout << "for example:\n\t\"knife;scissor\" \"d:/SCWObjectDetectConf.ini\" \"D:\\imagesA\" \"D:\\imagesB\"" << endl;
		return -1;
	}

	string strObjectType = argv[1];
	std::cout << "objectName: " << strObjectType << endl;
	string strConfigFile = argv[2];
	std::cout << "ConfigFile: " << strConfigFile << endl;

	std::cout << "Init, param 1: \"" << strObjectType <<  "\", param 2: \"" << strConfigFile <<  "\"" << endl;
	int nRet = DetectionInit(strObjectType.c_str(), strConfigFile.c_str());
	if (0 != nRet) {
		printf("fail to DetectionInit: %d", nRet);
		return -1;
	}

	int nMissErrorRatio = 5 < argc ? atoi(argv[5]) : 0;
	for (size_t i = 3; i < argc; i++) {
		string strImgDir = argv[i];
		std::cout << "image dir: " << strImgDir << endl;
		BatchProcess(strImgDir, nMissErrorRatio);
	}

	//printf(">>> process: %s\n", szImgFilePath);
	//char* pszRes = Detection(szImgFilePath, missErrorRatio);
	//printf("%s\n<---\n", pszRes);

	DetectionUnInit();

	::google::FlushLogFiles(0);
	::google::ShutdownGoogleLogging();
	printf("job finished\n");
	return 0;
}