#include <stdlib.h>
#include <direct.h>
#include <algorithm>
#include <windows.h>
#include "UtilFile.h"

using namespace std;

bool IsImageFileExt(const char * pszExt)
{
	if (_stricmp(pszExt, "jpg") == 0 ||
		_stricmp(pszExt, "jpeg") == 0 ||
		_stricmp(pszExt, "png") == 0 ||
		_stricmp(pszExt, "bmp") == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

#ifdef _WIN32

void SearchImageFiles(vector<string> & fileList, const char * pszImagePath, bool bFullPath) {
	//LOG_FUNCTION;
	if (NULL == pszImagePath)
	{
		return;
	}
	char szSearchPath[_MAX_PATH];
	strcpy_s(szSearchPath, pszImagePath);

	strcat_s(szSearchPath, "/*.*");

	HANDLE hFind;
	WIN32_FIND_DATAA ffd;

	hFind = FindFirstFileA(szSearchPath, &ffd);
	DWORD errorcode = 0;
	if (hFind == INVALID_HANDLE_VALUE) {
		printf("Can not find any file\n");
		return;
	}

	while (true)
	{
		if ((ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
		{
			int nLen = (int)strlen(ffd.cFileName);
			if (nLen >= 5)
			{
				char * pszExt = strrchr(ffd.cFileName, '.');

				if (pszExt != NULL && IsImageFileExt(pszExt + 1))
				{
					if (bFullPath)
					{
						string strFile = pszImagePath;
						strFile += "/";
						strFile += ffd.cFileName;
						fileList.push_back(strFile);
					}
					else
					{
						fileList.push_back(string(ffd.cFileName));
					}
				}
			}
		}

		if (FindNextFileA(hFind, &ffd) == FALSE)
			break;
	}

	FindClose(hFind);
}
#else
void SearchImageFiles(vector<string> & fileList, const char * pszImagePath, bool bFullPath = false)
{
	LOG_FUNCTION;
	char szSearchPath[_MAX_PATH];
	strcpy(szSearchPath, pszImagePath);

	DIR *dir = opendir(szSearchPath);
	if (dir == NULL)  {
		printf("Can not find any file\n");
		return;
	}

	while (true)
	{
		struct dirent *s_dir = readdir(dir);
		if (s_dir == NULL)
			break;

		if ((strcmp(s_dir->d_name, ".") == 0) || (strcmp(s_dir->d_name, "..") == 0))
			continue;

		char currfile[_MAX_PATH];
		sprintf(currfile, "%s/%s", szSearchPath, s_dir->d_name);
		struct stat file_stat;
		stat(currfile, &file_stat);
		if (!S_ISDIR(file_stat.st_mode))
		{
			int nLen = strlen(s_dir->d_name);
			if (nLen >= 5)
			{
				char * pszExt = strrchr(s_dir->d_name, '.');

				if (pszExt != NULL && IsImageFileExt(pszExt + 1))
				{
					if (bFullPath)
					{
						string strFile = pszImagePath;
						strFile += "/";
						strFile += s_dir->d_name;
						fileList.push_back(strFile);
					}
					else
					{
						fileList.push_back(string(s_dir->d_name));
					}

					printf("%s %s\n", currfile, s_dir->d_name);
				}
			}
		}
	}

	closedir(dir);
}
#endif

string replacePathSeparator(const char * pszFilePath)
{
	string strFile = pszFilePath;
	std::replace(strFile.begin(), strFile.end(), '/', '_');
	std::replace(strFile.begin(), strFile.end(), '\\', '_');
	std::replace(strFile.begin(), strFile.end(), ':', '_');
	return strFile;
}
