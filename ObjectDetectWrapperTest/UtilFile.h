#ifndef __UTILFILE_H__
#define __UTILFILE_H__

#include <vector>

void SearchImageFiles(std::vector<std::string> & fileList, const char * pszImagePath, bool bFullPath = false);

#endif 
