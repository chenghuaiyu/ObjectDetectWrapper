//#include "stdafx.h"

#include "AlgorithmTable.h"
#include <stdio.h>
#include <intrin.h>
#pragma intrinsic(_ReturnAddress)

#pragma warning(disable:4996)

char* strrstr(char* s1, char* set2)
{
	char* last=NULL;
	char* current=s1;
	if (*set2 != '\0')
	{
		while (current != NULL)   
		{
			last = current;
			current = strstr(last+1, set2);
		}
	}
	return last;
}