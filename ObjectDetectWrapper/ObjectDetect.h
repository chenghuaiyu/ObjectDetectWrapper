#ifndef _ALGORITHM_DISPATCHER_H_
#define _ALGORITHM_DISPATCHER_H_

#define ATR_API extern "C" _declspec(dllexport)

//return -1 no this object
ATR_API int DetectionInit(
	const char* objectsType,
	const char* deviceNumber
	);

ATR_API char* Detection(
	const char* imagesPath,
	int   missErrorRatio
	);

ATR_API void DetectionUnInit();

#endif