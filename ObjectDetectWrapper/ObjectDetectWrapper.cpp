//#include "stdafx.h"
#include <string>
#include <vector>
#include <direct.h>
#include <sys/stat.h>
#include <algorithm>
#include <functional>
#include <cctype>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <map>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/reader.h>
#include <rapidjson/writer.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/stringbuffer.h>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "ObjectDetect.h"
#include "AlgorithmTable.h"
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include "glog/logging.h"   // glog 头文件

using namespace std;
using namespace cv;

#define SHOW_MESSAGE

#define MAXOUTLENGHT   4000000//4M

#define SAVE_MERGE_RESULT	1
#define SAVE_ORIGINAL_RESULT	2

const unsigned __int64 magic = 0x6e192721b1b8;
__int64 tag = 0;

char** G_ObjectsList=NULL;
char* G_OutResult=NULL;
F_DetectInit* G_AlgorithmInitTable=NULL;
F_DetectCore* G_AlgorithmCoreTable=NULL;
F_DetectUnInit* G_AlgorithmUnInitTable=NULL;
HMODULE* G_AlgorithmHmodule=NULL;

char szObjSep[] = ";";

#ifndef MIN
#  define MIN(a,b)  ((a) > (b) ? (b) : (a))
#endif

#ifndef MAX
#  define MAX(a,b)  ((a) < (b) ? (b) : (a))
#endif

struct MyRect {
	int left;
	int top;
	int right;
	int bottom;
	MyRect(int l, int t, int r, int b){
		left = l;
		top = t;
		right = r;
		bottom = b;
	}
};

bool IsIntersect(MyRect & rc1, MyRect & rc2){
	return rc1.left < rc2.right && rc1.right > rc2.left && rc1.top < rc2.bottom && rc1.bottom > rc2.top;
}

MyRect UnionRect(MyRect & rc1, MyRect & rc2){
	return MyRect(MIN(rc1.left, rc2.left), MIN(rc1.top, rc2.top), MAX(rc1.right, rc2.right), MAX(rc1.bottom, rc2.bottom));
}

MyRect IntersectRect(MyRect & rc1, MyRect & rc2){
	return MyRect(MAX(rc1.left, rc2.left), MAX(rc1.top, rc2.top), MIN(rc1.right, rc2.right), MIN(rc1.bottom, rc2.bottom));
}

std::map<std::string, std::vector<std::pair<MyRect, int>>> MergeRect(std::map<std::string, std::vector<std::pair<MyRect, int>>> & mapObject) {
	for (map<string, vector<pair<MyRect, int>>>::iterator it = mapObject.begin(); it != mapObject.end(); it++) {
		string strObjType = it->first;
		vector<pair<MyRect, int>> & vecRect = it->second;
		for (int i = 0; i < vecRect.size(); i++) {
			for (int j = vecRect.size() - 1; i < j; j--) {
				if (IsIntersect(vecRect.at(i).first, vecRect.at(j).first)) {
					vecRect.at(i).first = UnionRect(vecRect.at(i).first, vecRect.at(j).first);
					vecRect.erase(vecRect.begin() + j);
				}
			}
		}
	}
	return mapObject;
}

#pragma region json
using namespace rapidjson;

std::string GetJsonString(rapidjson::Value &root) {
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	root.Accept(writer);
	std::string reststring = buffer.GetString();
	return reststring;
}

rapidjson::Value CreateResult(rapidjson::Document::AllocatorType& allocator, const char * pszImgName, const char * pszObjName, int width, int height) {
	rapidjson::Value image(rapidjson::kObjectType);
	image.AddMember("imgname", StringRef(pszImgName), allocator);
	image.AddMember("objecttype", StringRef(pszObjName), allocator);
	image.AddMember("width", width, allocator);
	image.AddMember("height", height, allocator);
	rapidjson::Value objectArray(rapidjson::kArrayType);
	image.AddMember("coords", objectArray, allocator);
	return image;
}

rapidjson::Value CreateCoordinate(rapidjson::Document::AllocatorType& allocator, MyRect rc) {
	int left = rc.left, top = rc.top, right = rc.right, bottom = rc.bottom;
	rapidjson::Value coord(rapidjson::kObjectType);
	coord.AddMember("x1", left, allocator);
	coord.AddMember("y1", top, allocator);
	coord.AddMember("x2", right, allocator);
	coord.AddMember("y2", top, allocator);
	coord.AddMember("x3", right, allocator);
	coord.AddMember("y3", bottom, allocator);
	coord.AddMember("x4", left, allocator);
	coord.AddMember("y4", bottom, allocator);
	return coord;
}

rapidjson::Value CreateRoot(rapidjson::Document & document) {
	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
	rapidjson::Value root(rapidjson::kObjectType);
	//root.AddMember("version", 1.1, allocator);
	rapidjson::Value resArray(rapidjson::kArrayType);
	root.AddMember("result", resArray, allocator);
	root.AddMember("suspect", 0, allocator);

	return root;
}

std::string outputJSON(const char * pszImgName, int width, int height, std::map<std::string, std::vector<std::pair<MyRect, int>>> mapObject) {
	rapidjson::Document document;
	rapidjson::Value root = CreateRoot(document);
	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
	rapidjson::Value & resArray = root["result"];
	if (0 == mapObject.size()) {
		rapidjson::Value & res = CreateResult(allocator, pszImgName, "", width, height);
		resArray.PushBack(res, allocator);
		return GetJsonString(root);
	}

	for (map<string, vector<pair<MyRect, int>>>::iterator it = mapObject.begin(); it != mapObject.end(); it++) {
		rapidjson::Value & res = CreateResult(allocator, pszImgName, it->first.c_str(), width, height);
		rapidjson::Value & coordsArr = res["coords"];

		vector<pair<MyRect, int>> vecRect = it->second;
		for (vector<pair<MyRect, int>>::iterator it2 = vecRect.begin(); it2 != vecRect.end(); it2++) {
			rapidjson::Value & coord = CreateCoordinate(allocator, it2->first);
			coordsArr.PushBack(coord, allocator);
		}

		resArray.PushBack(res, allocator);
	}

	return GetJsonString(root);
}

std::vector<std::pair<MyRect, int>> parse(rapidjson::Value &coordsArr) {
	std::vector<std::pair<MyRect, int>> vecRect;
	for (rapidjson::SizeType i = 0; i < coordsArr.Size(); i++) {
		const rapidjson::Value& coord = coordsArr[i];
		MyRect rc(0, 0, 0, 0);
		rc.left = coord["x1"].GetInt();
		rc.top = coord["y1"].GetInt();
		rc.right = coord["x2"].GetInt();
		rc.bottom = coord["y3"].GetInt();
		vecRect.push_back(std::pair<MyRect, int>(rc, 0));
	}
	return vecRect;
}

//std::string CombineJsonString(std::string str1, std::string str2) {
//	rapidjson::Document doc;
//	doc.Parse<0>(str1.c_str());
//
//	rapidjson::Document doc2;
//	doc2.Parse<0>(str2.c_str());
//
//	rapidjson::Value &resArr = doc["result"];
//	if (!resArr.IsArray()){
//		printf("first string format error\n");
//		return str2;
//	}
//	rapidjson::Value &resArr2 = doc2["result"];
//	if (!resArr2.IsArray()){
//		printf("second string format error\n");
//		return str1;
//	}
//	string strImgName = resArr[0]["imgname"].GetString();
//	int width = resArr[0]["width"].GetInt();
//	int height = resArr[0]["height"].GetInt();
//
//	std::map<std::string, std::vector<std::pair<MyRect, int>>> mapObject;
//	for (rapidjson::SizeType i = 0; i < resArr.Size(); i++) {
//		rapidjson::Value &res = resArr[i];
//		string strObjectType = res["objecttype"].GetString();
//		rapidjson::Value &coordsArr = res["coords"];
//		if (!coordsArr.IsArray()){
//			printf("coords format error\n");
//			return str2;
//		}
//
//		std::vector<std::pair<MyRect, int>>vecRect= parse(coordsArr);
//		if (0 == vecRect.size()) {
//			continue;
//		}
//		mapObject[strObjectType] = vecRect;
//	}
//	for (rapidjson::SizeType i = 0; i < resArr2.Size(); i++) {
//		rapidjson::Value &res = resArr2[i];
//		string strObjectType = res["objecttype"].GetString();
//		rapidjson::Value &coordsArr = res["coords"];
//		if (!coordsArr.IsArray()){
//			printf("coords format error\n");
//			return str2;
//		}
//
//		std::vector<std::pair<MyRect, int>>vecRect= parse(coordsArr);
//		if (0 == vecRect.size()) {
//			continue;
//		}
//		if (mapObject.find(strObjectType) == mapObject.end()) {
//			// not found
//			mapObject[strObjectType] = vecRect;
//		}
//		else {
//			// found
//			for (int i = 0; i < vecRect.size(); i++) {
//				mapObject[strObjectType].push_back(vecRect.at(i));
//			}
//		}
//	}
//
//	return outputJSON(strImgName.c_str(), width, height, mapObject);
//}
//
//std::string CombineJsonString(vector<string> vecRes, bool bMergeRect, int nShowResult) {
//	printf("--->\tCombineJsonString count: %d\n", vecRes.size());
//	if (0 == vecRes.size()) {
//		return "";
//	}
//	if (1 == vecRes.size()) {
//		return vecRes.at(0);
//	}
//	string strImgName = "";
//	int width = 0;
//	int height = 0;	
//	std::map<std::string, std::vector<std::pair<MyRect, int>>> mapObject;
//	for (int ind = 0; ind < vecRes.size(); ind++) {
//		rapidjson::Document doc;
//		doc.Parse<0>(vecRes.at(ind).c_str());
//
//		rapidjson::Value &resArr = doc["result"];
//		if (!resArr.IsArray()){
//			printf("No. %d string format error\n", ind);
//			vecRes.erase(vecRes.begin() + ind);
//			return vecRes.size() > 0 ?  vecRes.at(0): "";
//		}
//		if (strImgName.empty()) {
//			strImgName = resArr[0]["imgname"].GetString();
//			width = resArr[0]["width"].GetInt();
//			height = resArr[0]["height"].GetInt();
//		}
//
//		for (rapidjson::SizeType i = 0; i < resArr.Size(); i++) {
//			rapidjson::Value &res = resArr[i];
//			string strObjectType = res["objecttype"].GetString();
//			rapidjson::Value &coordsArr = res["coords"];
//			if (!coordsArr.IsArray()){
//				printf("No. %d-%d coords format error\n", ind, i);
//				vecRes.erase(vecRes.begin() + ind);
//				return vecRes.size() > 0 ? vecRes.at(0) : "";
//			}
//
//			std::vector<std::pair<MyRect, int>>vecRect = parse(coordsArr);
//			if (0 == vecRect.size()) {
//				continue;
//			}
//			if (mapObject.find(strObjectType) == mapObject.end()) {
//				// not found
//				mapObject[strObjectType] = vecRect;
//			}
//			else {
//				// found
//				for (int i = 0; i < vecRect.size(); i++) {
//					mapObject[strObjectType].push_back(vecRect.at(i));
//				}
//			}
//		}
//	}
//	if (bMergeRect) {
//		MergeRect(mapObject);
//	}
//	//if (nShowResult) {
//	//	if (bMergeRect && (1 & nShowResult)) {
//	//		// 显示合并后的结果
//	//		Mat m = imread(it->c_str());
//
//	//		if (nShowResult & 64) {
//	//		}
//	//	}
//
//	//	if (2 & nShowResult) {
//	//		// 显示每个算法的结果
//
//	//		if (nShowResult & 64) {
//	//		}
//	//	}
//	//}
//	return outputJSON(strImgName.c_str(), width, height, mapObject);
//}

bool GetImgInfoFromJsonString(string strJson, string & strImgName, int & width, int & height) {
#ifdef SHOW_MESSAGE
	//printf("--->\ JsonString: %s\n", strJson.c_str());
#endif
	rapidjson::Document doc;
	doc.Parse<0>(strJson.c_str());

	if (!doc.HasMember("result")) {
		LOG(ERROR) << "json string has not member: result";
		return false;
	}
	rapidjson::Value &resArr = doc["result"];
	if (!resArr.IsArray()){
		LOG(ERROR) << "string format error";
		return false;
	}

	rapidjson::Value &val1 = resArr[0];
	if (val1.IsNull()) {
		return false;
	}

	if (!val1.HasMember("imgname")) {
		LOG(ERROR) << "json string has not member: imgname";
		return false;
	}
	if (!val1.HasMember("width")) {
		LOG(ERROR) << "json string has not member: width";
		return false;
	}
	if (!val1.HasMember("height")) {
		LOG(ERROR) << "json string has not member: height";
		return false;
	}

	strImgName = resArr[0]["imgname"].GetString();
	width = resArr[0]["width"].GetInt();
	height = resArr[0]["height"].GetInt();

	return true;
}

bool FilterJsonString(string strJson, vector<string> & vecObjGT, vector<string> & vecObj, std::map<std::string, std::vector<std::pair<MyRect, int>>> & mapObject) {
#ifdef SHOW_MESSAGE
	//printf("--->\ JsonString: %s\n", strJson.c_str());
#endif

	rapidjson::Document doc;
	doc.Parse<0>(strJson.c_str());

	rapidjson::Value &resArr = doc["result"];
	if (!resArr.IsArray()){
		LOG(ERROR) << "string format error";
		return false;
	}

	//string strImgName = resArr[0]["imgname"].GetString();
	//int width = resArr[0]["width"].GetInt();
	//int height = resArr[0]["height"].GetInt();

	for (rapidjson::SizeType i = 0; i < resArr.Size(); i++) {
		rapidjson::Value &res = resArr[i];
		string strObjectType = res["objecttype"].GetString();
		if (vecObjGT.end() == std::find(vecObjGT.begin(), vecObjGT.end(), strObjectType)) {
			//printf("discard object type: %s\n", strObjectType.c_str());
			LOG(INFO) << "gt discard object type: " << strObjectType;
			continue;
		}
		if (vecObj.end() == std::find(vecObj.begin(), vecObj.end(), strObjectType)) {
			//printf("discard object type: %s\n", strObjectType.c_str());
			LOG(INFO) << "wrapper discard object type: " << strObjectType;
			continue;
		}

		rapidjson::Value &coordsArr = res["coords"];
		if (!coordsArr.IsArray()){
			//printf("No. %d coords format error\n", i);
			LOG(ERROR) << "coords format error";
			return false;
		}

		std::vector<std::pair<MyRect, int>> vecRect = parse(coordsArr);
		if (0 == vecRect.size()) {
			continue;
		}
		if (mapObject.find(strObjectType) == mapObject.end()) {
			// not found
			mapObject[strObjectType] = vecRect;
		} else {
			// found
			for (int i = 0; i < vecRect.size(); i++) {
				mapObject[strObjectType].push_back(vecRect.at(i));
			}
		}
	}
	
	return true;
}

std::map<std::string, std::vector<std::pair<MyRect, int>>> parseJsonString(vector<string> vecRes, string & strImgName, int & width, int & height) {
#ifdef SHOW_MESSAGE
	printf("--->\tJsonString count: %d\n", vecRes.size());
#endif
	std::map<std::string, std::vector<std::pair<MyRect, int>>> mapObject;
	if (0 == vecRes.size()) {
		return mapObject;
	}

	strImgName = "";
	width = 0;
	height = 0;
	for (int ind = 0; ind < vecRes.size(); ind++) {
		rapidjson::Document doc;
		doc.Parse<0>(vecRes.at(ind).c_str());

		rapidjson::Value &resArr = doc["result"];
		if (!resArr.IsArray()){
			//printf("No. %d string format error\n", ind);
			LOG(ERROR) << "string format error";
			vecRes.erase(vecRes.begin() + ind);
			return mapObject;
			//return vecRes.size() > 0 ? vecRes.at(0) : "";
		}
		if (strImgName.empty()) {
			strImgName = resArr[0]["imgname"].GetString();
			width = resArr[0]["width"].GetInt();
			height = resArr[0]["height"].GetInt();
		}

		for (rapidjson::SizeType i = 0; i < resArr.Size(); i++) {
			rapidjson::Value &res = resArr[i];
			string strObjectType = res["objecttype"].GetString();
			rapidjson::Value &coordsArr = res["coords"];
			if (!coordsArr.IsArray()){
				//printf("No. %d-%d coords format error\n", ind, i);
				LOG(ERROR) << "coords format error";
				vecRes.erase(vecRes.begin() + ind);
				return mapObject;
				//return vecRes.size() > 0 ? vecRes.at(0) : "";
			}

			std::vector<std::pair<MyRect, int>>vecRect = parse(coordsArr);
			if (0 == vecRect.size() && 0 < mapObject.size()) {
				continue;
			}
			if (mapObject.find(strObjectType) == mapObject.end()) {
				// not found
				mapObject[strObjectType] = vecRect;
			}
			else {
				// found
				for (int i = 0; i < vecRect.size(); i++) {
					mapObject[strObjectType].push_back(vecRect.at(i));
				}
			}
		}
	}
	return mapObject;
}

#pragma endregion json

#define	FLAG_ObjectType	"ObjectType"
#define	FLAG_ConfigPath	"ConfigPath"
#define	FLAG_MergeRect "MergeRect"
#define	FLAG_SaveResult	"SaveResult"
#define	FLAG_ResultPath	"ResultPath"
#define	FLAG_MissErrorRatio	"MissErrorRatio"
#define	FLAG_DllPath	"DllPath"
#define	FLAG_RemoveSmallRect "RemoveSmallRect"

struct SCWWrapperConf {
	bool bRemoveSmallRect;
	bool bMergeRect;
	int nSaveResult;
	char szResultFilePath[FILENAME_MAX];
};
SCWWrapperConf g_conf;
vector<DllFunc> g_vecFunc;
vector<string> g_vecObj;

HMODULE GetSelfModuleHandle() {
	MEMORY_BASIC_INFORMATION mbi;
	return ((::VirtualQuery(GetSelfModuleHandle, &mbi, sizeof(mbi)) != 0) ? (HMODULE)mbi.AllocationBase : NULL);
}

bool ReadConfigFile(const char * pszIniFile, vector<WrapperConf> & vecConfig, struct SCWWrapperConf & conf) {
	if (NULL == pszIniFile) {
		return false;
	}

	char szIniFileFullPath[_MAX_PATH];
	GetFullPathNameA(pszIniFile, _MAX_PATH, szIniFileFullPath, NULL);
	LOG(INFO) << "configure file: " << szIniFileFullPath;
	struct stat buffer;
	bool bFound = (stat(szIniFileFullPath, &buffer) == 0);
	if (!bFound){
		LOG(ERROR) << "configure file not found: " << szIniFileFullPath;
		return false;
	}

	char szSectionConf[_MAX_PATH] = "CONFIG";
	char szKeys[_MAX_PATH];
	DWORD nBytes = ::GetPrivateProfileSectionA(szSectionConf, szKeys, _MAX_PATH, szIniFileFullPath);
	if (0 == nBytes) {
		LOG(ERROR) << "fail to read configure section: " << szSectionConf;
		return false;
	}
	nBytes = ::GetPrivateProfileSectionA("DLL1", szKeys, _MAX_PATH, szIniFileFullPath);
	if (0 == nBytes) {
		LOG(ERROR) << "fail to read configure section: " << "DLL1";
		return false;
	}

	WrapperConf confGlobal = {};
	nBytes = ::GetPrivateProfileStringA(szSectionConf, FLAG_ObjectType, "", confGlobal.szObjectType, sizeof(confGlobal.szObjectType) - 1, szIniFileFullPath);
	//if (0 == nBytes) {
	//	LOG(INFO) << "fail to read global configure: " << FLAG_ObjectType;
	//}
	
	nBytes = ::GetPrivateProfileStringA(szSectionConf, FLAG_ConfigPath, "", confGlobal.szConfigPath, _MAX_PATH, szIniFileFullPath);
	//if (0 == nBytes) {
	//	LOG(INFO) << "fail to read global configure: " << FLAG_ConfigPath;
	//}
	
	//confGlobal.nMissErrorRatio=GetPrivateProfileIntA("CONFIG", FLAG_MissErrorRatio, 0, pszIniFile);
	int nMergeRect = GetPrivateProfileIntA(szSectionConf, FLAG_MergeRect, 0, szIniFileFullPath);
	conf.bMergeRect = nMergeRect != 0;
	LOG(INFO) << FLAG_ConfigPath << ": " << conf.bMergeRect;

	conf.bRemoveSmallRect = GetPrivateProfileIntA(szSectionConf, FLAG_RemoveSmallRect, 0, szIniFileFullPath) != 0;
	LOG(INFO) << FLAG_RemoveSmallRect << ": " << conf.bRemoveSmallRect;

	conf.nSaveResult = GetPrivateProfileIntA(szSectionConf, FLAG_SaveResult, 0, szIniFileFullPath);
	LOG(INFO) << FLAG_SaveResult << ": " << conf.nSaveResult;

	nBytes = ::GetPrivateProfileStringA(szSectionConf, FLAG_ResultPath, "", conf.szResultFilePath, _MAX_PATH, szIniFileFullPath);
	if (conf.nSaveResult && (0 == nBytes)) {
		LOG(ERROR) << "fail to read global configure: " << FLAG_ResultPath;
		return false;
	}
	LOG(INFO) << FLAG_ResultPath << ": " << conf.szResultFilePath;

	int nInd = 0;
	char szAppName[_MAX_PATH] = {};
	while (true) {
		nInd++;
		sprintf(szAppName, "DLL%d", nInd);
		nBytes = ::GetPrivateProfileSectionA(szAppName, szKeys, _MAX_PATH, szIniFileFullPath);
		if (0 == nBytes) {
			LOG(INFO) << "No configure section: " << szAppName;
			break;
		}

		WrapperConf conf;
		nBytes = ::GetPrivateProfileStringA(szAppName, FLAG_DllPath, "", conf.szDllPath, _MAX_PATH, szIniFileFullPath);
		if (0 == nBytes) {
			LOG(INFO) << "fail to read " << szAppName  << "'s configure: " << FLAG_DllPath;
			return false;
		}

		nBytes = ::GetPrivateProfileStringA(szAppName, FLAG_ObjectType, "", conf.szObjectType, sizeof(confGlobal.szObjectType) - 1, szIniFileFullPath);
		if (0 == nBytes) {
			if (strlen(confGlobal.szObjectType)) {
				strcpy(conf.szObjectType, confGlobal.szObjectType);
			} else{
				LOG(ERROR) << "fail to read " << szAppName  << "'s configure: " << FLAG_ObjectType;
				return false;
			}
		}

		nBytes = ::GetPrivateProfileStringA(szAppName, FLAG_ConfigPath, "", conf.szConfigPath, _MAX_PATH, szIniFileFullPath);
		if (0 == nBytes) {
			if (strlen(confGlobal.szConfigPath)) {
				strcpy(conf.szConfigPath, confGlobal.szConfigPath);
			} else{
				LOG(ERROR) << "fail to read " << szAppName  << " configure: " << FLAG_ConfigPath;
				return false;
			}
		}

		conf.nMissErrorRatio = ::GetPrivateProfileIntA(szAppName, FLAG_MissErrorRatio, 0, szIniFileFullPath);

		vecConfig.push_back(conf);
	}

	return vecConfig.size() > 0;
}

vector<DllFunc> GetProc(vector<WrapperConf> & vecConfig) {
	vector<DllFunc> vecFunc;
	for (int i = 0; i < vecConfig.size(); i++) {
		WrapperConf conf = vecConfig.at(i);
		LOG(INFO) << i << " load library: " << conf.szDllPath;
		HMODULE hDll = LoadLibraryExA(conf.szDllPath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
		if (hDll == NULL) {
			LOG(ERROR) << "fail to load library: " << conf.szDllPath;
			continue;
		}

		F_DetectInit init = (F_DetectInit)GetProcAddress(hDll, "DetectionInit");
		if (NULL == init) {
			FreeLibrary(hDll);
			LOG(ERROR) << "fail to load GetProcAddress DetectionInit: " << conf.szDllPath;
			continue;
		}
		F_DetectCore core = (F_DetectCore)GetProcAddress(hDll, "Detection");
		if (NULL == core) {
			FreeLibrary(hDll);
			LOG(ERROR) << "fail to load GetProcAddress Detection: " << conf.szDllPath;
			continue;
		}
		F_DetectUnInit unInit = (F_DetectUnInit)GetProcAddress(hDll, "DetectionUnInit");
		if (NULL == unInit) {
			FreeLibrary(hDll);
			LOG(ERROR) << "fail to load GetProcAddress DetectionUnInit: " << conf.szDllPath;
			continue;
		}
		LOG(INFO) << i << " successfully load library: " << conf.szDllPath;

		DllFunc df;
		df.conf = conf;
		df.hDll = hDll;
		df.init = init;
		df.core = core;
		df.unInit = unInit;
		vecFunc.push_back(df);
	}
	return vecFunc;
}
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

// trim from start (in place)
static inline void ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(),
		std::not1(std::ptr_fun<int, int>(std::isspace))));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(),
		std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
	ltrim(s);
	return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
	rtrim(s);
	return s;
}

// trim from both ends (in place)
static inline std::string trim_copy(std::string s) {
	return rtrim_copy(ltrim_copy(s));
}

vector<string> SplitObjects(const char* pszObjectType, const char* seps) {
	char szTemp[_MAX_PATH << 10] = {};
	strcpy(szTemp, pszObjectType);
	vector<string> vec;
	char *token = strtok(szTemp, seps);
	while (token != NULL) {
		string str = trim_copy(token);
		if (! str.empty()) {
			vec.push_back(str);
		}
		token = strtok(NULL, seps);
	}
	return vec;
}

bool IsInvalid(int nMin, int nMax) {
	//return (nMin <= 10) || (nMin <= 20 && nMax <= 40);
	//return (nMin <= 10) || (nMin <= 24 && nMax <= 55);
	//return (nMin <= 10) || (nMin <= 25 && nMax <= 55);
	return (nMin <= 10) || (nMin <= 15 && nMax <= 85)  || (nMin <= 20 && nMax <= 70)  || (nMin <= 25 && nMax <= 55) || (nMin <= 28 && nMax <= 50) || (nMin <= 30 && nMax <= 45) || (nMin <= 34 && nMax <= 44);
}

std::string GetFileNameByFilePathName(const std::string filePathName) {
	const int BUFSIZE = 4096;
	char buffer[BUFSIZE];
	char * lppPart = { NULL };
	DWORD retval = GetFullPathNameA(filePathName.c_str(), BUFSIZE, buffer, &lppPart);
	if (retval == 0) {
		// Handle an error condition.
		printf("GetFullPathName failed (%d)\n", GetLastError());
		return "";
	}

	std::string filename = buffer;
	if (filePathName.size() < 0)
		return "";
	string::size_type ix = filename.find_last_of('\\');

	if (ix != string::npos)
		filename = filename.substr(ix + 1, filename.size() - ix);

	return filename;
}

bool DirectoryExists(const char* pszPath) {
	//LOG(INFO) << "DirectoryExists(): \"" << pszPath << "\"";
	if (pszPath == NULL)
		return false;
	size_t n = strlen(pszPath) - 1;
	size_t ind = n;
	while (ind > 0 && (pszPath[ind] == '/' || pszPath[ind] == '\\')) {
		ind--;
	}
	char* pszPathNoSlash;
	char szTemp[_MAX_PATH] = {};
	if (ind < n) {
		strncpy(szTemp, pszPath, ind + 1);
		pszPathNoSlash = szTemp;
	}
	else {
		pszPathNoSlash = (char*)pszPath;
	}
	struct _stat st;
	int nRet = _stat(pszPathNoSlash, &st);
	if (nRet == 0)
		if ((st.st_mode & _S_IFDIR) != 0)
			return true;
	return false;
}

void string_replace(std::string&s1, const std::string&s2, const std::string&s3)
{
	std::string::size_type pos = 0;
	std::string::size_type a = s2.size();
	std::string::size_type b = s3.size();
	while ((pos = s1.find(s2, pos)) != std::string::npos)
	{
		s1.replace(pos, a, s3);
		pos += b;
	}
}

std::string convertPath2Name(const char * pszImgName) {
	char szFullPath[_MAX_PATH];
	GetFullPathNameA(pszImgName, _MAX_PATH, szFullPath, NULL);
	std::string strFile = szFullPath;

	string_replace(strFile, "/", "_");
	string_replace(strFile, "\\", "_");
	string_replace(strFile, ":", "__");
	return strFile;
}

/*
1,define what objects will detect;
2,define which devices maybe fit;
3,define which algorithms will use;
4,load resources for all images;
5,open memory for all images;

for all images,these things will only run once.
*/
/// objectsType: define the object types to be detected in the image
/// pszConfigFile: 
ATR_API int DetectionInit(
	const char* objectsType,
	const char* pszConfigFile
	) {
	//string str = "{\"result\":[{\"imgname\":\"E:/images/samples-27641/20170908_090802672.jpg\",\"objecttype\":\"knife\",\"width\":244,\"height\":216,\"coords\":[{\"x1\":202,\"y1\":95,\"x2\":224,\"y2\":95,\"x3\":202,\"y3\":129,\"x4\":224,\"y4\":129}]}]}";
	//string str2 = "{\"result\":[{\"imgname\":\"D:\\Logic_zyzh\\20170911_140837118.jpg\",\"objecttype\":\"gun\",\"width\":318,\"height\":217,\"coords\":[]},{\"imgname\":\"D:\\Logic_zyzh\\20170911_140837118.jpg\",\"objecttype\":\"hammer_axe\",\"width\":318,\"height\":217,\"coords\":[]},{\"imgname\":\"D:\\Logic_zyzh\\20170911_140837118.jpg\",\"objecttype\":\"knife\",\"width\":318,\"height\":217,\"coords\":[{\"x1\":39,\"y1\":24,\"x2\":113,\"y2\":24,\"x3\":113,\"y3\":73,\"x4\":39,\"y4\":73}]},{\"imgname\":\"D:\\Logic_zyzh\\20170911_140837118.jpg\",\"objecttype\":\"lei_guan\",\"width\":318,\"height\":217,\"coords\":[]},{\"imgname\":\"D:\\Logic_zyzh\\20170911_140837118.jpg\",\"objecttype\":\"pen_wu\",\"width\":318,\"height\":217,\"coords\":[]},{\"imgname\":\"D:\\Logic_zyzh\\20170911_140837118.jpg\",\"objecttype\":\"shuang_zhi\",\"width\":318,\"height\":217,\"coords\":[]}],\"suspect\":20}";
	////std::string res = CombineJsonString( str, str2);
	//vector<string> vecStr;
	//vecStr.push_back(str);
	//vecStr.push_back(str2);
	//std::string res = CombineJsonString(vecStr);

	//::google::InitGoogleLogging("ObjectDetectWrapper");
	LOG(INFO) << "DetectionInit param 1: \"" << objectsType << "\", param 2: \""<< pszConfigFile << "\"";
	if (tag == magic) {
		LOG(ERROR) << "This function should be invoked only once.";
		return -1;
	}
	LOG(INFO) << "Split Objects with: " << szObjSep;
	g_vecObj = SplitObjects(objectsType, szObjSep);
	LOG(INFO) << "object type count: " << g_vecObj.size();
	if (0 == g_vecObj.size()) {
		LOG(ERROR) << "none object type found.";
		return -1;
	}
	sort(g_vecObj.begin(), g_vecObj.end());
	g_vecObj.erase(unique(g_vecObj.begin(), g_vecObj.end()), g_vecObj.end());
	LOG(INFO) << "object type unique count: " << g_vecObj.size();

	char szConfPath[_MAX_PATH] = { 0 };
	GetFullPathNameA(pszConfigFile, _MAX_PATH, szConfPath, NULL);
	LOG(INFO) << "ConfigFile: " << szConfPath;

	struct stat buffer;
	bool bFound = (stat(pszConfigFile, &buffer) == 0);
	if (!bFound) {
		LOG(WARNING) << "using default file path due to the configure file not found: " << pszConfigFile;

		ifstream file;
		char szDllPath[_MAX_PATH] = { 0 };
		GetModuleFileNameA(GetSelfModuleHandle(), szDllPath, 100);
		LOG(INFO) << "Dll Path:" << szDllPath;
		const char * pszIniFile = "SCWObjectDetectConf.ini";
		char* pSep = strrchr(szDllPath, '\\');
		if (NULL == pSep) {
			LOG(ERROR) << "'\\' char not found: " << szDllPath;
			return -1;
		}
		pSep[1] = '\0';
		strcat(szDllPath, pszIniFile);
		sprintf(szConfPath, szDllPath);
		LOG(INFO) << "default configure file: " << szDllPath;
	}

	//char szIniFileFullPath[_MAX_PATH];
	//GetFullPathNameA(szDllPath, _MAX_PATH, szIniFileFullPath, NULL);
	//struct stat buffer;
	//bool bFound = (stat(pszIniFile, &buffer) == 0);
	//if (!bFound) {
	//	LOG(ERROR) << "configure file not found: " << szDllPath;
	//	return -1;
	//}

	char szDLLFolder[MAX_PATH + 1];
	vector<WrapperConf> vecConfig;
	bool bRet = ReadConfigFile(szConfPath, vecConfig, g_conf);
	if (!bRet) {
		LOG(ERROR) << "fail to read configure item in file: " << szConfPath;
		return -1;
	}
	g_vecFunc = GetProc(vecConfig);
	if (0 == g_vecFunc.size()) {
		LOG(ERROR) << "fail to load library in conf file: " << szConfPath;
		return -2;
	}
	LOG(INFO) << "Dll configure count: " << vecConfig.size() << ", successfully load count:" << g_vecFunc.size();

	for (int i = 0; i < g_vecFunc.size(); i++) {
		DllFunc df = g_vecFunc.at(i);
		LOG(INFO) << i << "> DetectionInit param 1: \"" << df.conf.szObjectType << "\", param 2: \"" << df.conf.szConfigPath << "\"";
		int nRet = df.init(df.conf.szObjectType, df.conf.szConfigPath);
		if (0 != nRet) {
			LOG(ERROR) << i << " fail to invoke DetectionInit: ";
			return -3;
		}
		LOG(INFO) << i << "> DetectionInit succefully";
	}

	G_OutResult = (char*)malloc(sizeof(char)*MAXOUTLENGHT);
	memset(G_OutResult, 0, sizeof(char)*MAXOUTLENGHT);
	tag = magic;

	::google::FlushLogFiles(0);
	return 0;
}

//release memory,and clear all value.
ATR_API void DetectionUnInit() {
	LOG(INFO) << "DetectionUnInit";

	if (tag == 0) {
		LOG(ERROR) << "DetectionUnInit not need to be invoked.";
		return ;
	}
	if (tag != magic) {
		LOG(ERROR) << "DetectionUnInit not need to be invoked.";
		return ;
	}
	tag = 0;

	for (int i = 0; i < g_vecFunc.size(); i++) {
		DllFunc df = g_vecFunc.at(i);
		df.unInit();
		FreeLibrary(df.hDll);
	}
	g_vecFunc.clear();
	//::google::ShutdownGoogleLogging();
	return;
}

ATR_API char* Detection(
	const char* pszImagePath,
	int   nErrorRatio
	) {
	LOG(INFO) << "Detection image: " << pszImagePath << ", missErrorRatio:" << nErrorRatio;
	if (g_conf.nSaveResult) {
		if (!DirectoryExists(g_conf.szResultFilePath)) {
			int nRet = _mkdir(g_conf.szResultFilePath);
			if (0 != nRet) {
				LOG(ERROR) << "return: " << nRet << " invoking _mkdir() " << g_conf.szResultFilePath;
			}
		}
	}

	char szImgPath[_MAX_PATH] = {};
	strcpy(szImgPath, pszImagePath);
	Scalar scalar = Scalar(0, 0, 255);
	string strFilename = convertPath2Name(pszImagePath);
	vector<string> vecRes;
	string strImgName;
	int width = 0;
	int height = 0;
	bool bGetImgInfo = false;
	std::map<std::string, std::vector<std::pair<MyRect, int>>> mapObject;
	for (int i = 0; i < g_vecFunc.size(); i++) {
		DllFunc df = g_vecFunc.at(i);
		char* pszRes = df.core(szImgPath, df.conf.nMissErrorRatio);
#ifdef SHOW_MESSAGE
		printf("%d>==================\n%s\n%d<==================\n", i + 1, pszRes, i + 1);
#endif
		if (strlen(pszRes)) {
			if (!bGetImgInfo) {
				if (!GetImgInfoFromJsonString(pszRes, strImgName, width, height)) {
					continue;
				}
				bGetImgInfo = true;
			}

			// filter result by object type
			vector<string> vecObjType = SplitObjects(df.conf.szObjectType, szObjSep);
			std::map<std::string, std::vector<std::pair<MyRect, int>>> mapObjectType;
			if (! FilterJsonString(pszRes, vecObjType, g_vecObj, mapObjectType)) {
				continue;
			}

			for (std::map<std::string, std::vector<std::pair<MyRect, int>>>::iterator iter = mapObjectType.begin(); iter != mapObjectType.end(); iter++) {
				std::string strObjectType = iter->first;
				std::vector<std::pair<MyRect, int>> vecPair = iter->second;
				if (mapObject.find(strObjectType) == mapObject.end()) {
					mapObject[strObjectType] = vecPair;
				} else {
					for (int i = 0; i < vecPair.size(); i++) {
						mapObject[strObjectType].push_back(vecPair.at(i));
					}
				}
			}

			if (mapObjectType.size() > 0 && (g_conf.nSaveResult & SAVE_ORIGINAL_RESULT)) {
				string strDir = g_conf.szResultFilePath + string("/");
				if (! DirectoryExists(strDir.c_str())) {
					int nRet = _mkdir(strDir.c_str());
					if (0 != nRet) {
						LOG(ERROR) << "return: " << nRet << " invoking _mkdir() " << strDir;
					}
				}

				char c = i < 9 ? i + '1' : i - 9 + 'A';
				strDir += c;
				if (! DirectoryExists(strDir.c_str())) {
					int nRet = _mkdir(strDir.c_str());
					if (0 != nRet) {
						LOG(ERROR) << "return: " << nRet << " invoking _mkdir() " << strDir;
					}
				}
				//LOG(INFO) << "return: " << nRet << " invoking _mkdir() " << strDir;
				Mat m = imread(pszImagePath);
				for (std::map<std::string, std::vector<std::pair<MyRect, int>>>::iterator iter = mapObjectType.begin(); iter != mapObjectType.end(); iter++) {
					Mat mObj = imread(pszImagePath);
					std::string obj = iter->first;
					std::vector<std::pair<MyRect, int>> vecPair = iter->second;
					for (size_t i = 0; i < vecPair.size(); i++) {
						MyRect rc = vecPair.at(i).first;
						rectangle(mObj, Point(rc.left, rc.top), Point(rc.right, rc.bottom), scalar);
						putText(mObj, obj, Point(rc.left, rc.top), cv::FONT_HERSHEY_PLAIN, 1, scalar);

						rectangle(m, Point(rc.left, rc.top), Point(rc.right, rc.bottom), scalar);
						putText(m, obj, Point(rc.left, rc.top), cv::FONT_HERSHEY_PLAIN, 1, scalar);
					}
					string strFile = strDir + string("/") + obj;
					if (! DirectoryExists(strFile.c_str())) {
						int nRet = _mkdir(strFile.c_str());
						if (0 != nRet) {
							LOG(ERROR) << "return: " << nRet << " invoking _mkdir() " << strFile;
						}
					}
					strFile += string("/") + strFilename + ".png";
					bool bRet = imwrite(strFile, mObj);
				}
				bool bRet = imwrite(strDir + string("/") + strFilename + ".png", m);
			}
		}
	}
	if (!bGetImgInfo) {
		LOG(ERROR) << "fail to GetImgInfoFromJsonString().";
		return "";
	}
	
	if (g_conf.bMergeRect) {
		MergeRect(mapObject);
	}

	// remove small rect
	if (g_conf.bRemoveSmallRect) {
		for (map<string, vector<pair<MyRect, int>>>::iterator it = mapObject.begin(); it != mapObject.end(); it++) {
			string strObjType = it->first;
			vector<pair<MyRect, int>> & vecRect = it->second;
			for (int i = 0; i < vecRect.size(); i++) {
				MyRect mr = vecRect.at(i).first;
				int nLen1 = mr.right - mr.left;
				int nLen2 = mr.bottom - mr.top;
				if (IsInvalid(MIN(nLen1, nLen2), MAX(nLen1, nLen2))){
					vecRect.erase(vecRect.begin() + i);
					i--;
				}
			}

			if (0 == vecRect.size()) {
				it = mapObject.erase(it);
			}
		}
	}

	if (mapObject.size() > 0 && g_conf.nSaveResult) {
		if (SAVE_MERGE_RESULT & g_conf.nSaveResult) {
			// 显示合并后的结果
			string strFile = g_conf.szResultFilePath + string("/");
			strFile += strFilename + ".png";
			Mat m = imread(pszImagePath);
			for (std::map<std::string, std::vector<std::pair<MyRect, int>>>::iterator iter = mapObject.begin(); iter != mapObject.end(); iter++) {
				std::string obj = iter->first;
				std::vector<std::pair<MyRect, int>> vecPair = iter->second;
				for (size_t i = 0; i < vecPair.size(); i++) {
					MyRect rc = vecPair.at(i).first;
					rectangle(m, Point(rc.left, rc.top), Point(rc.right, rc.bottom), scalar);
					putText(m, obj, Point(rc.left, rc.top), cv::FONT_HERSHEY_PLAIN, 1, scalar);
				}
			}
			imwrite(strFile, m);
		}
	}
	string strComb = outputJSON(strImgName.c_str(), width, height, mapObject);
	if (g_conf.nSaveResult) {
		string filePathName = g_conf.szResultFilePath + string("/") + strFilename + ".txt";
		std::ofstream ofs(filePathName, std::ofstream::out);
		ofs << strComb;
		ofs.close();
	}

	//sprintf(G_OutResult, "%s", strComb, "\"suspect\":", suspect, "}");
	strcpy(G_OutResult, strComb.c_str());
#ifdef SHOW_MESSAGE
	printf("\n\nwrapper result: %s\n\n", strComb.c_str());
#endif

	::google::FlushLogFiles(0);
	return G_OutResult;
}