#ifndef _ALGORITHM_TABLE_H_
#define _ALGORITHM_TABLE_H_

#include <Windows.h>

//Algorithm Table
//each line have all object types.a line only mean a device.
//x line means :there will be x devices.
//return pointer of one line.
typedef char* charp;
typedef int (*F_DetectInit) (char* objectsType, char* deviceNumber);
typedef charp (*F_DetectCore) (char* imagesPath, int missErrorRatio);
typedef void (*F_DetectUnInit) ();

struct WrapperConf	{
	char szDllPath[_MAX_PATH + 1];
	char szObjectType[_MAX_PATH << 8];
	char szConfigPath[_MAX_PATH + 1];
	int nMissErrorRatio;
};

struct DllFunc {
	struct WrapperConf conf;
	HMODULE hDll;
	F_DetectInit init;
	F_DetectCore core;
	F_DetectUnInit unInit;
};

char* strrstr(char* s1, char* set2);
#endif